"""baseline

Revision ID: 60f36ebf31ea
Revises: 
Create Date: 2018-11-07 16:45:37.421547

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '60f36ebf31ea'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'user',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('username', sa.String(500), unique=True, nullable=False),
        sa.Column('password', sa.String(500), unique=False, nullable=False)
    )
    op.create_table(
        'post',
         sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
         sa.Column('author_id', sa.Integer, nullable=False),
         sa.Column('created', sa.DateTime(), server_default=sa.FetchedValue()),
         sa.Column('title', sa.String(500), nullable=False),
         sa.Column('body', sa.String(5000), nullable=False)
    )


def downgrade():
    op.drop_table('user')
    op.drop_table('post')
