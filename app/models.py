from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import func
from sqlalchemy import Column, DateTime, Integer, String, FetchedValue

Base = declarative_base()


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(), unique=True, nullable=False)
    password = Column(String(), unique=False, nullable=False)

    def __repr__(self):
        return "<User(id='{}', name='{}', password='{}')>"\
            .format(self.id, self.username, self.password)

    def as_dict(self):
        return {
            'id': self.id,
            'username': self.username,
            'password': self.password,
        }


class Post(Base):
    __tablename__ = 'post'

    id = Column(Integer, primary_key=True, autoincrement=True)
    author_id = Column(Integer, nullable=False)
    created = Column(DateTime(), nullable=False, server_default=func.now())
    title = Column(String(), nullable=False)
    body = Column(String(), nullable=False)

    def __repr__(self):
        return "<Post(id='{}', title='{}', created='{}', author_id='{}')>"\
            .format(self.id, self.title, self.created, self.author_id)

    def as_dict(self):
        return {
            'id': self.id,
            'title': self.title,
            'body': self.body,
            'created': self.created,
            'author_id': self.author_id,
        }