import os

from flask import Flask
from flask import jsonify
from flask import current_app, g
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

def get_db_uri():
    user = os.environ['MYSQL_USER']
    password = os.environ['MYSQL_PASSWORD']
    database = os.environ['MYSQL_DATABASE']
    hostname = os.environ['MYSQL_HOSTNAME']
    return 'mysql+pymysql://' + user + ':' + password + '@' + hostname + '/' + database

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    db_uri = get_db_uri()
    #engine = create_engine('sqlite:///instance/flaskr.sqlite', echo=True)
    engine = create_engine(db_uri, echo=True)
    Session = sessionmaker(bind=engine)
    app.config.from_mapping(
        SECRET_KEY='dev',
        #DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
        #SQLALCHEMY_DATABASE_URI='sqlite:///'+os.path.join(app.instance_path, 'flaskr.sqlite'),
        #SQLALCHEMY_DATABASE_URI='mysql://user:redhat@db/blog',
        ALCHEMY_SESSION=Session,
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    #from . import db
    #from app.models import db
    #db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import blog
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')

    @app.route('/test')
    def test_endpoint():
        from app.models import Post
        session = current_app.config['ALCHEMY_SESSION']()

        return "\n".join(list(map(lambda x: str(x), session.query(Post).order_by(Post.id))))

    return app
