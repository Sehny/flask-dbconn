from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from app.auth import login_required
from app.db import get_db
from app.models import Post, User

bp = Blueprint('blog', __name__)


@bp.route('/')
def index():
    db = get_db()
    posts = list(map(lambda x: x.as_dict(), db.query(Post).order_by(Post.created)))
    users = list(map(lambda x: x.as_dict(), db.query(User).all()))
    users = {x['id']: x['username'] for x in users}
    posts = [dict({'user': users[x['author_id']]}, **x) for x in posts]
    print(posts)
    return render_template('blog/index.html', posts=posts)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            new_post = Post(title=title, body=body, author_id=g.user['id'])
            db.add(new_post)
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/create.html')


def get_post(id, check_author=True):
    # post = get_db().execute(
    #     'SELECT p.id, title, body, created, author_id, username'
    #     ' FROM post p JOIN user u ON p.author_id = u.id'
    #     ' WHERE p.id = ?',
    #     (id,)
    # ).fetchone()
    post = get_db().query(Post).filter_by(id=id).first().as_dict()

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post


@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            # db.execute(
            #     'UPDATE post SET title = ?, body = ?'
            #     ' WHERE id = ?',
            #     (title, body, id)
            # )
            post = db.query(Post).filter_by(id=id).first()
            post.title = title
            post.body = body
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/update.html', post=post)


@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_post(id)
    db = get_db()
    post = db.query(Post).filter_by(id=id).first()
    db.delete(post)
    db.commit()
    return redirect(url_for('blog.index'))