from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from app.models import Post, User

engine = create_engine('sqlite:///instance/flaskr.sqlite', echo=True)
Session = sessionmaker(bind=engine)
session = Session()

for instance in session.query(Post).order_by(Post.id):
    print(instance)

for instance in session.query(User).order_by(User.id):
    print(instance)

session.close()