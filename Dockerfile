FROM fedora:latest

RUN dnf install python3-devel mysql-devel redhat-rpm-config gcc -y

RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app/requirements.txt

RUN pip3 install -r requirements.txt &&\
    pip3 install gunicorn PyMySQL

COPY . /app
EXPOSE 8080
CMD ["/usr/local/bin/gunicorn", "-w", "1", "app:create_app()", "-b", "0.0.0.0:8080"]
